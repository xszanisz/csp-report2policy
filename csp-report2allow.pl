#!/usr/bin/perl

use warnings;
use strict;

use Data::Dumper;
use JSON -support_by_pp;
use Getopt::Long;

our $verbose;
our $violation_lines;

sub parse_csp_rules {
    my %params = @_;

    my $location = $params{'location'};

    my $new_csp_rules = {};

    while (my $csp_json_string = <>) {
        print "---\n" if $verbose;

        my $csp_json = eval { decode_json($csp_json_string) };
        if ($@) {
            print STDERR "error at line $.: failed to parse JSON:\n\t$csp_json_string";
            next;
        }

        my $blocked_uri = $csp_json->{'csp-report'}->{'blocked-uri'};
        my $violated_directive = $csp_json->{'csp-report'}->{'violated-directive'};
        my $effective_directive = $csp_json->{'csp-report'}->{'effective-directive'};
        my $script_sample = $csp_json->{'csp-report'}->{'script-sample'};

        my $directive_to_fix = $effective_directive // $violated_directive;

        if (not (defined $blocked_uri and defined $directive_to_fix)) {
            print STDERR "error at line $.: missing blocked-uri or both of violated-directive and effective-directive:\n\t$csp_json_string";
            next;
        }

        # this can contain only the directive name or the whole directive
        $directive_to_fix = (split / /, $directive_to_fix)[0];

        print "line $.: blocked-uri=$blocked_uri violated_directive=" . ($violated_directive // '<undef>') . " effective_directive=" . ($effective_directive // '<undef>') . " directive_to_fix=$directive_to_fix\n" if $verbose;

        my $uri_to_allow;
        # self, eval, data, asset, inline
        if ($blocked_uri =~ m{^([^/]*://[^/]*)}) {
            my $uri = $1;
            if (defined $location and $uri eq $location) {
                $uri_to_allow = "'self'";
            } else {
                $uri_to_allow = $1;
            }
        } elsif ($blocked_uri eq 'eval') {
            $uri_to_allow = "'unsafe-eval'";
        } elsif ($blocked_uri eq 'inline') {
            $uri_to_allow = "'unsafe-inline'";

        # self
        } elsif ($blocked_uri eq 'self' and $script_sample eq 'call to eval() or related function blocked by CSP') {
            $uri_to_allow = "'unsafe-eval'";
        } elsif ($blocked_uri eq 'self' and $script_sample =~ m/^on.* attribute on .* element$/) {
            $uri_to_allow = "'unsafe-inline'";
        } elsif ($blocked_uri eq 'self') {
            $uri_to_allow = "'unsafe-inline'";

        } elsif ($blocked_uri eq 'data') {
            $uri_to_allow = "data:";
        } elsif ($blocked_uri eq '') {
            $uri_to_allow = "TODO_empty_uri";
        } elsif ($blocked_uri eq 'asset') {
            $uri_to_allow = "TODO_uri_asset";
        } elsif ($blocked_uri eq 'ws') {
            $uri_to_allow = "ws:";
        } elsif ($blocked_uri eq 'wss') {
            $uri_to_allow = "wss:";
        } elsif ($blocked_uri eq 'about') {
            $uri_to_allow = "TODO_uri_about";
        } else {
            print STDERR "error at line $.: unknown blocked_uri type $blocked_uri:\n\t$csp_json_string";
            next;
        }

        if ($violation_lines) {
            push @{ $new_csp_rules->{$directive_to_fix}->{$uri_to_allow} }, $.;
        } else {
            $new_csp_rules->{$directive_to_fix}->{$uri_to_allow} += 1;
        }

    }

    return $new_csp_rules;
}

sub prepare_policy {
    my ($new_csp_rules) = @_;

    my $policy = '';
    while (my ($dir_name, $directive) = each %$new_csp_rules) {

        # throw default-src away because it is too broad and overrides other directives
        next if $dir_name eq 'default-src';

        $policy .= $dir_name . ' ' . (join ' ', keys %$directive) . '; ';
    }

    $policy =~ s/ $//;

    return $policy;
}

sub help {
    print STDERR <<EOF;
--location LOCATION     URI of web location that is subject to CSP policy. Used to substitute 'self' in policy generation where possible
--verbose               Show more information about processed lines
--violating-lines, -n   For each new policy show lineno's where violations occurred instead of the count of violations
EOF
    exit 1;
}

my $location = undef;
GetOptions(
    "location=s" => \$location,
    "verbose" => \$verbose,
    "violation-lines|n" => \$violation_lines,
) or help();

my $rules = parse_csp_rules('location' => $location);
$Data::Dumper::Useqq = 1;
print Dumper($rules);
print prepare_policy($rules) . "\n";
